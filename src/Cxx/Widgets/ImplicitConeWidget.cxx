#include "vtkActor.h"
#include "vtkAppendPolyData.h"
#include "vtkCamera.h"
#include "vtkClipPolyData.h"
#include "vtkCommand.h"
#include "vtkCone.h"
#include "vtkConeSource.h"
#include "vtkGlyph3D.h"
#include "vtkImplicitConeRepresentation.h"
#include "vtkImplicitConeWidget.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSphereSource.h"
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>

#include <array>

namespace {

/**
 * The callback for the interaction.
 *
 * This does the actual work: updates the vtkCone implicit function.
 * This in turn causes the pipeline to update and clip the object.
 *
 * @return
 */
class vtkTICWCallback : public vtkCommand
{
public:
  static vtkTICWCallback* New()
  {
    return new vtkTICWCallback;
  }

  void Execute(vtkObject* caller, unsigned long, void*) override
  {
    vtkImplicitConeWidget* coneWidget =
        reinterpret_cast<vtkImplicitConeWidget*>(caller);
    vtkImplicitConeRepresentation* rep =
        reinterpret_cast<vtkImplicitConeRepresentation*>(
            coneWidget->GetRepresentation());
    rep->GetCone(this->cone);
    this->actor->VisibilityOn();
  }

  vtkCone* cone = nullptr;
  vtkActor* actor = nullptr;
};

} // anonymous namespace

int main(int argc, char* argv[])
{
  std::array<unsigned char, 4> bkg{82, 87, 110, 255};
  vtkNew<vtkNamedColors> colors;
  colors->SetColor("ParaViewBkg", bkg.data());

  // Create a mace out of filters.
  vtkNew<vtkSphereSource> sphere;
  vtkNew<vtkConeSource> coneSource;
  vtkNew<vtkGlyph3D> glyph;
  glyph->SetInputConnection(sphere->GetOutputPort());
  glyph->SetSourceConnection(coneSource->GetOutputPort());
  glyph->SetVectorModeToUseNormal();
  glyph->SetScaleModeToScaleByVector();
  glyph->SetScaleFactor(0.25);
  glyph->Update();

  // The sphere and spikes are appended into a single polydata.
  // This just makes things simpler to manage.
  vtkNew<vtkAppendPolyData> apd;
  apd->AddInputConnection(glyph->GetOutputPort());
  apd->AddInputConnection(sphere->GetOutputPort());

  vtkNew<vtkPolyDataMapper> maceMapper;
  maceMapper->SetInputConnection(apd->GetOutputPort());

  vtkNew<vtkActor> maceActor;
  maceActor->SetMapper(maceMapper);
  maceActor->GetProperty()->SetColor(
      colors->GetColor3d("LightSteelBlue").GetData());
  maceActor->VisibilityOn();

  // This portion of the code clips the mace with the vtkCone's
  // implicit function. The clipped region is colored green.
  vtkNew<vtkCone> cone;
  cone->SetIsDoubleCone(false);

  vtkNew<vtkClipPolyData> clipper;
  clipper->SetInputConnection(apd->GetOutputPort());
  clipper->SetClipFunction(cone);
  clipper->InsideOutOn();

  vtkNew<vtkPolyDataMapper> selectMapper;
  selectMapper->SetInputConnection(clipper->GetOutputPort());

  vtkNew<vtkActor> selectActor;
  selectActor->SetMapper(selectMapper);
  selectActor->GetProperty()->SetColor(colors->GetColor3d("Lime").GetData());
  selectActor->VisibilityOff();
  selectActor->SetScale(1.01, 1.01, 1.01);

  // Create the RenderWindow, Renderer and both Actors
  vtkNew<vtkRenderer> renderer;
  renderer->AddActor(maceActor);
  renderer->AddActor(selectActor);
  renderer->SetBackground(colors->GetColor3d("ParaViewBkg").GetData());

  vtkNew<vtkRenderWindow> renWin;
  renWin->SetMultiSamples(0);
  renWin->SetSize(640, 640);
  renWin->AddRenderer(renderer);

  vtkNew<vtkRenderWindowInteractor> interactor;
  renWin->SetInteractor(interactor);
  auto is =
      vtkInteractorStyleSwitch::SafeDownCast(interactor->GetInteractorStyle());
  if (is)
  {
    is->SetCurrentStyleToTrackballCamera();
  }

  // The SetInteractor method is how 3D widgets are associated with the render
  // window interactor. Internally, SetInteractor sets up a bunch of callbacks
  // using the Command/Observer mechanism (AddObserver()).
  vtkNew<vtkTICWCallback> myCallback;
  myCallback->cone = cone;
  myCallback->actor = selectActor;

  vtkNew<vtkImplicitConeRepresentation> rep;
  rep->SetPlaceFactor(1.25);
  rep->PlaceWidget(glyph->GetOutput()->GetBounds());

  vtkNew<vtkImplicitConeWidget> coneWidget;
  coneWidget->SetInteractor(interactor);
  coneWidget->SetRepresentation(rep);
  coneWidget->AddObserver(vtkCommand::InteractionEvent, myCallback);
  coneWidget->SetEnabled(true);

  interactor->Initialize();
  renderer->ResetCamera();
  renderer->GetActiveCamera()->Elevation(30);
  renderer->GetActiveCamera()->Azimuth(30);
  renderer->GetActiveCamera()->SetRoll(-22.5);
  renderer->ResetCamera();
  renderer->GetActiveCamera()->Zoom(0.65);

  interactor->Start();
  return EXIT_SUCCESS;
}
 
