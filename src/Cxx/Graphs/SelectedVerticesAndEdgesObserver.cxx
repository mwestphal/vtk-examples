#include <vtkAnnotationLink.h>
#include <vtkCallbackCommand.h>
#include <vtkGraphLayoutView.h>
#include <vtkIdTypeArray.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkRandomGraphSource.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderedGraphRepresentation.h>
#include <vtkRenderer.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>

namespace {

void SelectionCallbackFunction(vtkObject* caller, long unsigned int eventId,
                               void* clientData, void* callData);
}

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;

  vtkNew<vtkRandomGraphSource> source;

  vtkNew<vtkGraphLayoutView> view;
  view->AddRepresentationFromInputConnection(source->GetOutputPort());

  view->GetRenderWindow()->SetSize(600, 600);
  view->GetRenderWindow()->SetWindowName("SelectedVerticesAndEdges");
  view->GetRenderer()->SetBackground(
      colors->GetColor3d("MidnightBlue").GetData());
  view->GetRenderer()->SetBackground2(
      colors->GetColor3d("RoyalBlue").GetData());

  vtkNew<vtkCallbackCommand> selectionCallback;
  selectionCallback->SetCallback(SelectionCallbackFunction);
  // The vtkRenderedGraphRepresentation should already have a vtkAnnotationLink,
  // so we just want to grab it and add an observer with our callback function
  // attached.
  view->GetRepresentation()->GetAnnotationLink()->AddObserver(
      "AnnotationChangedEvent", selectionCallback);

  view->ResetCamera();
  view->Render();

  view->GetInteractor()->Start();

  return EXIT_SUCCESS;
}

namespace {

void SelectionCallbackFunction(vtkObject* caller, long unsigned int eventId,
                               void* clientData, void* callData)
{

  vtkAnnotationLink* annotationLink = static_cast<vtkAnnotationLink*>(caller);

  vtkSelection* selection = annotationLink->GetCurrentSelection();
  vtkSelectionNode* vertices;
  vtkSelectionNode* edges;
  if (selection->GetNode(0)->GetFieldType() == vtkSelectionNode::VERTEX)
  {
    vertices = selection->GetNode(0);
  }
  else if (selection->GetNode(0)->GetFieldType() == vtkSelectionNode::EDGE)
  {
    edges = selection->GetNode(0);
  }

  if (selection->GetNode(1)->GetFieldType() == vtkSelectionNode::VERTEX)
  {
    vertices = selection->GetNode(1);
  }
  else if (selection->GetNode(1)->GetFieldType() == vtkSelectionNode::EDGE)
  {
    edges = selection->GetNode(1);
  }

  vtkIdTypeArray* vertexList =
      dynamic_cast<vtkIdTypeArray*>(vertices->GetSelectionList());
  std::cout << "There are " << vertexList->GetNumberOfTuples()
            << " vertices selected." << std::endl;
  auto hasVertices = vertexList->GetNumberOfTuples() > 0;
  if (hasVertices)
  {
    std::cout << "Vertex Ids: ";
    for (vtkIdType i = 0; i < vertexList->GetNumberOfTuples(); i++)
    {
      if (i < vertexList->GetNumberOfTuples() - 1)
      {
        std::cout << vertexList->GetValue(i) << ", ";
      }
      else
      {
        std::cout << vertexList->GetValue(i) << std::endl;
      }
    }
  }

  vtkIdTypeArray* edgeList =
      dynamic_cast<vtkIdTypeArray*>(edges->GetSelectionList());
  std::cout << "There are " << edgeList->GetNumberOfTuples()
            << " edges selected." << std::endl;
  auto hasEdges = edgeList->GetNumberOfTuples() > 0;
  if (hasEdges)
  {
    std::cout << "Edge Ids: ";

    for (vtkIdType i = 0; i < edgeList->GetNumberOfTuples(); i++)
    {
      if (i < edgeList->GetNumberOfTuples() - 1)
      {
        std::cout << edgeList->GetValue(i) << ", ";
      }
      else
      {
        std::cout << edgeList->GetValue(i) << std::endl;
      }
    }
  }

  if (hasVertices or hasEdges)
  {
    std::cout << "- - -" << std::endl;
  }
  else
  {
    std::cout << std::endl;
  }
}

} // namespace
