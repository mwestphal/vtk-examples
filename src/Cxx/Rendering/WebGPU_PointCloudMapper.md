### Description

This example demonstrates how to use the `vtkWebGPUPointCloudMapper` of the WebGPU module with colors
mapped on points through scalars.

This mapper derives from `vtkPolyDataMapper` and is thus used analogously. The mapper will only
render points (and not 1-vertex cells) of the polydata it is given as input. 

The colors can be set on points through the use of scalars.

Note that to use this example, you will need your VTK to be built with WebGPU. Building instructions
can be found [here](https://docs.vtk.org/en/latest/modules/vtk-modules/Rendering/WebGPU/README.html).

It is expected that the illustration image of this example is flipped compared to what is rendered on
screen when running the example locally.
