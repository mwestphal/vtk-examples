#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkCubeAxesActor.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTextProperty.h>

#include <iostream>
#include <string>

int main(int, char*[])
{
  vtkNew<vtkNamedColors> colors;

  // Create a renderer, render window and interctor.
  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetWindowName("DetermineActorType");

  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Create a sphere.
  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->Update();

  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(sphereSource->GetOutputPort());

  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper);
  actor->GetProperty()->SetColor(colors->GetColor3d("MistyRose").GetData());

  // Cube axes.
  vtkNew<vtkCubeAxesActor> cubeAxesActor;

  cubeAxesActor->SetCamera(renderer->GetActiveCamera());

  cubeAxesActor->GetXAxesTitleProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetYAxesTitleProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetZAxesTitleProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetXAxesLinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetYAxesLinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetZAxesLinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetXAxesLabelProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetYAxesLabelProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetZAxesLabelProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetXAxesGridlinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetYAxesGridlinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());
  cubeAxesActor->GetZAxesGridlinesProperty()->SetColor(
      colors->GetColor3d("Gold").GetData());

  cubeAxesActor->DrawXGridlinesOff();
  cubeAxesActor->DrawYGridlinesOff();
  cubeAxesActor->DrawZGridlinesOff();
  cubeAxesActor->SetGridLineLocation(vtkCubeAxesActor::VTK_GRID_LINES_FURTHEST);

  cubeAxesActor->XAxisMinorTickVisibilityOn();
  cubeAxesActor->YAxisMinorTickVisibilityOn();
  cubeAxesActor->ZAxisMinorTickVisibilityOn();

  // cubeAxesActor->SetFlyMode(vtkCubeAxesActor::VTK_FLY_STATIC_EDGES);

  renderer->AddActor(actor);
  renderer->AddActor(cubeAxesActor);
  renderer->SetBackground(colors->GetColor3d("DarkSlateGray").GetData());

  std::string wantedClass = "vtkCubeAxesActor";
  vtkActorCollection* actorCollection = renderer->GetActors();

  // Determine the types of the actors - method 1.
  {
    std::cout << "Method 1:" << std::endl;
    vtkActorCollection* actorCollection = renderer->GetActors();
    actorCollection->InitTraversal();

    for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
      vtkActor* nextActor = actorCollection->GetNextActor();
      std::cout << "nextActor " << i << " : " << nextActor->GetClassName()
                << std::endl;
      std::string className = nextActor->GetClassName();
      if (className == wantedClass)
      {
        std::cout << "nextActor " << i << " is a " << wantedClass << "!"
                  << std::endl;
      }
      else
      {
        std::cout << "nextActor " << i << " is NOT a" << wantedClass << "!"
                  << std::endl;
      }
    }
  }

  // Determine the types of the actors - method 2.
  {
    std::cout << "Method 2:" << std::endl;
    actorCollection->InitTraversal();

    for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
      vtkActor* nextActor = actorCollection->GetNextActor();
      std::cout << "nextActor " << i << " : " << nextActor->GetClassName()
                << std::endl;
      if (nextActor->IsA("vtkCubeAxesActor"))
      {
        std::cout << "nextActor " << i << " is a " << wantedClass << "!"
                  << std::endl;
      }
      else
      {
        std::cout << "nextActor " << i << " is NOT a " << wantedClass << "!"
                  << std::endl;
      }
    }
  }

  // Determine the types of the actors - method 3.
  {
    std::cout << "Method 3:" << std::endl;
    actorCollection->InitTraversal();

    for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
      vtkActor* nextActor = actorCollection->GetNextActor();
      std::cout << "nextActor " << i << " : " << nextActor->GetClassName()
                << std::endl;

      if (dynamic_cast<vtkCubeAxesActor*>(nextActor) != 0)
      {
        std::cout << "nextActor " << i << " is a " << wantedClass << "!"
                  << std::endl;
      }
      else
      {
        std::cout << "nextActor " << i << " is NOT a " << wantedClass << "!"
                  << std::endl;
      }
    }
  }

  // Determine the types of the actors - method 4.
  {
    std::cout << "Method 4:" << std::endl;
    actorCollection->InitTraversal();

    for (vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
      vtkActor* nextActor = actorCollection->GetNextActor();
      std::cout << "nextActor " << i << " : " << nextActor->GetClassName()
                << std::endl;

      if (dynamic_cast<vtkCubeAxesActor*>(nextActor) != 0)
      {
        std::cout << "nextActor " << i << " is a " << wantedClass << "!"
                  << std::endl;
      }
      else
      {
        std::cout << "nextActor " << i << " is NOT a " << wantedClass << "!"
                  << std::endl;
      }
    }
  }

  // Render the scene.
  renderWindow->AddRenderer(renderer);

  renderWindow->Render();
  renderer->ResetCamera();
  auto camera = renderer->GetActiveCamera();
  camera->SetPosition(0, 0, 8.09748);
  camera->SetFocalPoint(0, 0, 0);
  camera->SetViewUp(0, 1, 0);
  camera->SetDistance(8.09748);
  camera->SetClippingRange(6.0265, 10.7239);

  renderWindowInteractor->Initialize();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
