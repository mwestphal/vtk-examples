#!/usr/bin/env python3

# This example demonstrates how to use the vtkCameraOrientationWidget to control
# a renderer's camera orientation.

from pathlib import Path

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkIOXML import vtkXMLPolyDataReader
from vtkmodules.vtkInteractionWidgets import vtkCameraOrientationWidget
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkPolyDataMapper,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkRenderer
)


def get_program_parameters():
    import argparse
    description = 'Demonstrates a 3D camera orientation widget.'
    epilogue = '''
    '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('path', help='The path to the file to render e.g. cow.vtp.')
    args = parser.parse_args()
    return args.path


def main():
    colors = vtkNamedColors()

    path = get_program_parameters()
    if not Path(path).is_file():
        print('Unable to find the file:', path)
        return

    ren = vtkRenderer(background=colors.GetColor3d('DimGray'))
    ren_win = vtkRenderWindow(size=(600, 600), window_name='CameraOrientationWidget')
    ren_win.AddRenderer(ren)
    iren = vtkRenderWindowInteractor()
    # Important: The interactor must be set prior to enabling the widget.
    iren.render_window = ren_win
    # Since we import vtkmodules.vtkInteractionStyle we can do this
    # because vtkInteractorStyleSwitch is automatically imported:
    iren.interactor_style.SetCurrentStyleToTrackballCamera()

    reader = vtkXMLPolyDataReader(file_name=path)

    mapper = vtkPolyDataMapper()
    reader >> mapper

    actor = vtkActor(mapper=mapper)
    actor.property.color = colors.GetColor3d('Beige')

    ren.AddActor(actor)

    cam_orient_manipulator = vtkCameraOrientationWidget(parent_renderer=ren,
                                                        interactor=iren)
    # Enable the widget.
    cam_orient_manipulator.On()

    ren_win.Render()
    iren.Initialize()
    iren.Start()


if __name__ == '__main__':
    main()
