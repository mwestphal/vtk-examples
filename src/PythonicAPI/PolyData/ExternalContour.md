### Description

Compute the external contour of a polydata.

At first, it creates a black and white image of a scene containing the polydata and then extracts the contour of the black shape from the image.

!!! note
    A longstanding bug is fixed, see the discussion [here](https://gitlab.kitware.com/vtk/vtk/-/issues/19412).
