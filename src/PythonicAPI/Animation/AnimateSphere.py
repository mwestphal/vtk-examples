#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkCommonCore import (
    vtkAnimationCue,
    vtkCallbackCommand
)
from vtkmodules.vtkCommonDataModel import vtkAnimationScene
from vtkmodules.vtkFiltersCore import vtkFeatureEdges
from vtkmodules.vtkFiltersSources import vtkSphereSource
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkPolyDataMapper,
    vtkProperty,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkRenderer,
)


def main():
    colors = vtkNamedColors()
    sphere_color = colors.GetColor3d('Gold')
    background_color = colors.GetColor3d('DarkSlateBlue')
    backface_color = colors.GetColor3d('DarkSlateGray')

    # Create the Renderer, RenderWindow and RenderWindowInteractor.
    ren = vtkRenderer(background=background_color)
    ren_win = vtkRenderWindow(window_name='AnimateSphere')
    ren_win.AddRenderer(ren)
    iren = vtkRenderWindowInteractor()
    iren.render_window = ren_win

    scene = vtkAnimationScene(loop=0, frame_rate=30, start_time=0, end_time=10)
    scene.SetModeToSequence()

    cue = vtkAnimationCue(start_time=0, end_time=1.0)
    cue.SetTimeModeToNormalized()

    sphere_backface_property = vtkProperty(color=backface_color)
    sphere_property = vtkProperty(color=sphere_color, diffuse=0.8, specular=0.3, specular_power=30.0)

    # Create a sphere.
    sphere = vtkSphereSource(phi_resolution=31, theta_resolution=31)
    mapper = vtkPolyDataMapper()
    sphere >> mapper
    actor = vtkActor(mapper=mapper, property=sphere_property, backface_property=sphere_backface_property)

    feature_edges = vtkFeatureEdges(boundary_edges=True, feature_edges=False, manifold_edges=False,
                                    non_manifold_edges=False,
                                    coloring=True)

    edge_mapper = vtkPolyDataMapper()
    sphere >> feature_edges >> edge_mapper
    edge_actor = vtkActor(mapper=edge_mapper)

    ren.AddActor(actor)
    ren.AddActor(edge_actor)

    sphere_animator = SphereAnimator(ren_win, sphere)
    cue.AddObserver('AnimationCueTickEvent', sphere_animator)

    scene.AddCue(cue)

    ren_win.Render()
    ren.active_camera.Yaw(45)
    ren.active_camera.Pitch(-15)
    ren.ResetCamera()
    iren.Initialize()

    scene.Play()
    scene.Stop()

    iren.Start()


class SphereAnimator(vtkCallbackCommand):
    def __init__(self, ren_win, sphere):
        self.ren_win = ren_win
        self.sphere = sphere
        super().__init__()

    def __call__(self, obj, event):
        new_st = obj.animation_time * 90
        self.sphere.start_theta = new_st
        self.ren_win.Render()


if __name__ == '__main__':
    main()
