#!/usr/bin/env python3

# noinspection PyUnresolvedReferences
import vtkmodules.vtkInteractionStyle
# noinspection PyUnresolvedReferences
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonColor import vtkNamedColors
from vtkmodules.vtkCommonCore import vtkAnimationCue, vtkCallbackCommand
from vtkmodules.vtkCommonDataModel import vtkAnimationScene
from vtkmodules.vtkFiltersSources import vtkSphereSource
from vtkmodules.vtkRenderingCore import (
    vtkActor,
    vtkPolyDataMapper,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkRenderer, vtkProperty
)


def main():
    colors = vtkNamedColors()
    background_color = colors.GetColor3d('MistyRose')

    # Create the Renderer, RenderWindow and RenderWindowInteractor.
    ren = vtkRenderer(background=background_color)
    ren_win = vtkRenderWindow(window_name='AnimateSphere')
    ren_win.AddRenderer(ren)
    iren = vtkRenderWindowInteractor()
    iren.render_window = ren_win

    ren_win.Render()

    scene = vtkAnimationScene(loop=0, frame_rate=5, start_time=3, end_time=20)
    # scene.SetModeToSequence()
    scene.SetModeToRealTime()

    cue = vtkAnimationCue(start_time=5, end_time=23)
    scene.AddCue(cue)

    # Create cue animator
    animator = CueAnimator()

    # Create Cue observer.
    observer = AnimationCueObserver(ren, ren_win, animator)

    cue.AddObserver('StartAnimationCueEvent', observer)
    cue.AddObserver('EndAnimationCueEvent', observer)
    cue.AddObserver('AnimationCueTickEvent', observer)

    scene.Play()
    scene.Stop()

    iren.Start()


class CueAnimator:
    def __init__(self):
        self.source = None
        self.mapper = None
        self.actor = None

    def start_cue(self, info, ren):
        colors = vtkNamedColors()

        sphere_property = vtkProperty(color=colors.GetColor3d('Peacock'), specular=0.6, specular_power=30.0)

        # Create a sphere.
        self.source = vtkSphereSource(radius=0.5)
        self.mapper = vtkPolyDataMapper()
        self.source >> self.mapper
        self.actor = vtkActor(mapper=self.mapper, property=sphere_property)

        ren.AddActor(self.actor)
        ren.ResetCamera()
        ren.Render()

    def tick(self, info, ren):
        new_radius = 0.1 + (info.animation_time - info.start_time) / (info.end_time - info.start_time) * 1
        # print(f'animation: {info.animation_time:5.2f}, start: {info.start_time:5.2f},'
        #       f' end: {info.end_time:5.2f}, radius: {new_radius:5.2f}')
        self.source.SetRadius(new_radius)
        self.source.update()
        ren.Render()

    def end_cue(self, info, ren):
        # Don't remove the actor for the regression image.
        # ren.RemoveActor(self.actor)
        pass


class AnimationCueObserver(vtkCallbackCommand):
    def __init__(self, renderer, ren_win, animator):
        self.ren_win = ren_win
        self.renderer = renderer
        self.animator = animator
        super().__init__()

    def __call__(self, info, event):
        if self.animator and self.renderer:
            if event == 'StartAnimationCueEvent':
                self.animator.start_cue(info, self.renderer)
            if event == 'AnimationCueTickEvent':
                self.animator.tick(info, self.renderer)
            if event == 'EndAnimationCueEvent':
                self.animator.end_cue(info, self.renderer)
        if self.ren_win:
            self.ren_win.Render()


if __name__ == '__main__':
    main()
